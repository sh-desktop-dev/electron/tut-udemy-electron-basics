const electron = require('electron');
const url = require('url');
const path = require('path');

const {app,BrowserWindow} = electron;

let mainWindow;

app.on('ready', appReady);

function appReady(){
    mainWindow = new BrowserWindow({
        show: false,
        width: 500,
        height:200,
        frame: false
    });
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.once('ready-to-show', ()=>{
        mainWindow.show();
    });
    mainWindow.on('closed', ()=>{
        mainWindow = null;
    });
    app.on('window-all-closed', ()=>{
        if(process.platform !== 'darwin'){
            app.quit();
        }
    })
}