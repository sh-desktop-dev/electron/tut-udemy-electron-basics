console.log('From one.js');

const BrowserWindow = require('electron').remote.BrowserWindow;
const path = require('path');
const url = require('url');

let newWinBtn = document.getElementById('newWin');
newWinBtn.addEventListener('click', (event)=>{
    let win3 = new BrowserWindow();
    win3.loadURL(url.format({
        pathname: path.join(__dirname, 'index3.html'),
        protocol: 'file:',
        slashes: true
    }));
});