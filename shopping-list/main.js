const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

//Set ENV
process.env.NODE_ENV = 'production';

let mainWindow, addWindow;

//Listen for the app to be ready
app.on('ready', createWindow);

function createWindow(){
 //create new window
 mainWindow = new BrowserWindow();
    
 //load html file
 mainWindow.loadURL(url.format({
     pathname: path.join(__dirname,'mainwindow.html'),
     protocol: 'file:',
     slashes: true
 }));
 //Quit app when closed
 mainWindow.on('closed', ()=>{
     app.quit();
 });

 //Build menu from the template
 const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
 //Insert menu
 Menu.setApplicationMenu(mainMenu);
}

//Handle create add window
function createAddWindow(){
    addWindow = new BrowserWindow({width: 300,
        height: 200,
        title: 'Add Shopping List Item'
    });
    
 //load html file
 addWindow.loadURL(url.format({
     pathname: path.join(__dirname,'addwindow.html'),
     protocol: 'file:',
     slashes: true
 }));

 //Garbage collection handling
 addWindow.on('close', ()=>{
     addWindow = null;
 });
}

//Catch item:add
ipcMain.on('item:add', (e, item)=>{
    console.log(item);
    mainWindow.webContents.send('item:add', item);
    addWindow.close();
})

//create menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Add Item',
                click(){
                    createAddWindow();
                }
            },
            {
                label: 'Clear Items',
                click(){
                    mainWindow.webContents.send('item:clear');
                }
            },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click(){
                    app.quit();
                }
            }
        ]
    }
];

//If MAC, add an empty object to menu
if(process.platform == 'darwin'){
    mainMenuTemplate.unshift({});   //unshift() adds to beginning of the array.
}

//Add developer toold item if not in production
if(process.env.NODE_ENV !== 'production'){
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'Toggle Dev Tools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
                
            }
        ]
    })
}